package esboot.model.es.vo;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class EsResSetVo implements Serializable {

	private static final long serialVersionUID = 1L;

	private long totalHitCount;
	
	private List<Map<String, Object>> listMapSrchResp;

	public long getTotalHitCount() {
		return totalHitCount;
	}

	public void setTotalHitCount(long totalHitCount) {
		this.totalHitCount = totalHitCount;
	}

	public List<Map<String, Object>> getListMapSrchResp() {
		return listMapSrchResp;
	}

	public void setListMapSrchResp(List<Map<String, Object>> listMapSrchResp) {
		this.listMapSrchResp = listMapSrchResp;
	}

}
