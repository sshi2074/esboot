package esboot.controller;

import java.util.Date;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class HomeController {

	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
    
	@RequestMapping(value="/", method= RequestMethod.GET)
	public String getHome(Locale locale, Model model) {
		
		logger.debug("# home started -");
		logger.debug("# locale: {} ", locale);
		
		Date date = new Date();
		model.addAttribute("maSysDate", date);		
		return "home/index";
	}
	
	
}
