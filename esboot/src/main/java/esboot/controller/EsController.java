package esboot.controller;

import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import esboot.service.EsService;

@Controller
public class EsController {

	@Autowired
	EsService svcEs;
	
	private static final Logger logger = LoggerFactory.getLogger(EsController.class);
    
    @RequestMapping(value = "/es/index/list", method = RequestMethod.GET)
    public String getIndexList(Locale locale, Model model) throws Exception {      	       
        /* Get All Index */
        model.addAttribute("maListIndex", svcEs.getListIndex());                        
        return "es/indexList";
    }		 
	
}
