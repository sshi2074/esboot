package esboot.controller;

import java.util.Locale;
import java.util.Map;

import org.elasticsearch.search.sort.SortOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import esboot.model.es.vo.EsResSetVo;
import esboot.model.form.Bbs;
import esboot.service.EsSampleService;
import esboot.service.EsService;

@Controller
public class EsSampleCrudController {

	@Autowired
	EsService svcEs;
	
	@Autowired
	EsSampleService svcEsSample;
	
	private static final Logger logger = LoggerFactory.getLogger(EsSampleCrudController.class);
    
    @RequestMapping(value = "/es/sample/list", method = RequestMethod.GET)
    public String getIndexList(Locale locale, Model model) throws Exception { 
    	model.addAttribute("maListBbs", svcEsSample.list().getListMapSrchResp());
        return "es/sample/list";
    }		 

    
    @RequestMapping(value = "/es/sample/kr/list", method = RequestMethod.GET)
    public String getIndexKrSampleList(Locale locale, Model model) throws Exception {     	
    	EsResSetVo rsSetVo = svcEsSample.getSrchApiResultSet("MATCH_ALL", "idx-kr-token-filter", "bbs", "tk_note", "", "date_created", SortOrder.DESC, 100, 0);    	    
    	model.addAttribute("maListBbs", rsSetVo.getListMapSrchResp());
        return "es/sample/listKr";
    }		 
    
    
    @RequestMapping(value = "/es/sample/qry/{cmd}", method = RequestMethod.POST)
    public String getQueryList(@PathVariable("cmd") String argCmd, @RequestParam("qryKey") String argSrchKey, Locale locale, Model model) throws Exception { 
    	EsResSetVo rsVo = svcEsSample.getSrchApiResultSetMatchTokenOption(argCmd, "test-openkor", "bbs", "tk_note", argSrchKey, "an_open_kor",100, 0);    	    	
    	model.addAttribute("maListBbs", rsVo.getListMapSrchResp());
    	model.addAttribute("maSrchKey", argSrchKey);
        return "es/sample/list";
    }		 
    
    
    @RequestMapping(value = "/es/sample/kr/qry/{cmd}", method = RequestMethod.GET)
    public String getQueryKrList(@PathVariable("cmd") String argCmd, @RequestParam("qryKey") String argSrchKey, Locale locale, Model model) throws Exception { 
    	EsResSetVo rsVo = svcEsSample.getSrchApiResultSetMatchTokenOption(argCmd, "idx-kr-token-filter", "bbs", "tk_note", argSrchKey, "an_open_kor_tock_phrase",100, 0);    	    	
    	model.addAttribute("maListBbs", rsVo.getListMapSrchResp());
    	model.addAttribute("maSrchKey", argSrchKey);
        return "es/sample/listKr";
    }	
    
    
    @RequestMapping(value = "/es/sample/edit/{id}", method = RequestMethod.GET)
    public String getEditForm(@PathVariable("id") String argId, Locale locale, Model model) throws Exception {      	       
    	
    	if(argId == null || argId.equals("0") || argId.equals("") ) {    		
    		Bbs bbs = new Bbs();
    		bbs.setId("0");
    		model.addAttribute("maBbs", bbs);
    		
    	} else {    		
    		Bbs bbs = svcEsSample.getDocBbs(argId);
    		model.addAttribute("maBbs", bbs);

    	}
    	    	
        return "es/sample/edit";
    }		 
    
    @RequestMapping(value = "/es/sample/kr/edit/{id}", method = RequestMethod.GET)
    public String getEditKrForm(@PathVariable("id") String argId, Locale locale, Model model) throws Exception {      	       
    	
    	if(argId == null || argId.equals("0") || argId.equals("") ) {    		
    		Bbs bbs = new Bbs();
    		bbs.setId("0");
    		model.addAttribute("maBbs", bbs);
    		
    	} else {  /* 일반적인 서비스 형태로 만들기 위해 변형 */    		    		
    		Map<String,Object> mapSrc = svcEsSample.getDoc("idx-kr-token-filter", "bbs",argId);
    		
    		Bbs bbs = new Bbs();
    		bbs.setId((String)mapSrc.get("_id"));
    		bbs.setSubject((String)mapSrc.get("subject"));
    		bbs.setNote((String)mapSrc.get("note"));    		
    		
    		model.addAttribute("maBbs", bbs);    		    		
    	}
    	    	
        return "es/sample/editKr";
    }	    
    
    @RequestMapping(value = "/es/sample/edit/{id}", method = RequestMethod.POST)
    public String setEditForm(@PathVariable("id") String argId, @ModelAttribute("maBbs") Bbs maBbs, Locale locale, Model model) throws Exception {
    	
    	logger.debug("# argId:" + argId);
    	logger.debug("# maBbs.getSubject():" + maBbs.getSubject());
    	
    	svcEsSample.insert(maBbs);
    	
    	return "redirect:/es/sample/list";
    }

    @RequestMapping(value = "/es/sample/kr/edit/{id}", method = RequestMethod.POST)
    public String setEditKrForm(@PathVariable("id") String argId, @ModelAttribute("maBbs") Bbs maBbs, Locale locale, Model model) throws Exception {
    	
    	logger.debug("# argId:" + argId);
    	logger.debug("# maBbs.getSubject():" + maBbs.getSubject());
    	
    	svcEsSample.save("idx-kr-token-filter", "bbs", maBbs);
    	
    	return "redirect:/es/sample/kr/list";
    }    
    
        
    @RequestMapping(value = "/es/sample/delete/{id}", method = RequestMethod.POST)
    public String deleteEditForm(@PathVariable("id") String argId, @ModelAttribute("maBbs") Bbs maBbs, Locale locale, Model model) throws Exception {
    	
    	logger.debug("# detelte request - argId:" + argId);
    	
    	if(argId!=null && !argId.equals("0") && !argId.equals("")) {
    		svcEsSample.delete(argId);
    	}
    	    	
    	return "redirect:/es/sample/list";
    }

    
    @RequestMapping(value = "/es/sample/kr/delete/{id}", method = RequestMethod.POST)
    public String deleteEditKrForm(@PathVariable("id") String argId, @ModelAttribute("maBbs") Bbs maBbs, Locale locale, Model model) throws Exception {
    	
    	logger.debug("# detelte request - argId:" + argId);
    	
    	if(argId!=null && !argId.equals("0") && !argId.equals("")) {
    		svcEsSample.delete("idx-kr-token-filter", "bbs", argId);
    	}
    	    	
    	return "redirect:/es/sample/kr/list";
    }
    
}
