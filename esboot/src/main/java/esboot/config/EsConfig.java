package esboot.config;

import java.net.InetAddress;

import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

@Configuration
public class EsConfig {

	@Autowired
    private Environment env;
        
    private static final Logger logger = LoggerFactory.getLogger(EsConfig.class);
    
    @Bean
    public TransportClient trnsportClient() throws Exception {
    	
    	logger.debug("# env.getProperty(es.cluster.name): " + env.getProperty("es.cluster.name"));
    	logger.debug("# env.getProperty(es.hostname): " + env.getProperty("es.hostname"));
    	
    	Settings settings = Settings.builder().put("cluster.name", env.getProperty("es.cluster.name")).build();

    	TransportClient client = new PreBuiltTransportClient(settings).addTransportAddress(
    			new InetSocketTransportAddress(InetAddress.getByName(env.getProperty("es.hostname")), 9300));
    	    	
    	return client;
    }    
	
}
