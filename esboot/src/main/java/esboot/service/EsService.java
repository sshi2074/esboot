package esboot.service;

import java.util.ArrayList;
import java.util.List;

import org.elasticsearch.client.transport.TransportClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EsService {
	
	@Autowired
	TransportClient svcEsClient;
	
	private static final Logger logger = LoggerFactory.getLogger(EsService.class);
	
	/*
	 * 	Get Index List from Elasticserch
	 */	
	public List<String> getListIndex() throws Exception {
		
		/* ES command: get /_cat/indices?v */
				
		List<String> listIndex = new ArrayList<String>();
		
		String[] strIndexSet = svcEsClient.admin().cluster().prepareState().execute().actionGet().getState().getMetaData().getConcreteAllIndices();
		
		logger.debug("# index size(): " + strIndexSet.length);
		
		for(String s: strIndexSet) {
			listIndex.add(s);
		}
		
		return listIndex;		
	}

}
