package esboot.service;

import static org.elasticsearch.common.xcontent.XContentFactory.jsonBuilder;
import static org.elasticsearch.index.query.QueryBuilders.matchAllQuery;
import static org.elasticsearch.index.query.QueryBuilders.matchQuery;
import static org.elasticsearch.index.query.QueryBuilders.regexpQuery;
import static org.elasticsearch.index.query.QueryBuilders.termQuery;
import static org.elasticsearch.index.query.QueryBuilders.wildcardQuery;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import esboot.model.es.vo.EsResSetVo;
import esboot.model.form.Bbs;

/**
 * 
 * @author gram
 *  using static import: 
 *   - import static org.elasticsearch.common.xcontent.XContentFactory.*;
 *   - import static org.elasticsearch.index.query.QueryBuilders*;
 */

@Service
public class EsSampleService {
		
	@Autowired
	TransportClient svcEsClient;
	
	private static final Logger logger = LoggerFactory.getLogger(EsSampleService.class);
	
	public EsResSetVo list() throws Exception {
		
		EsResSetVo rsSetVo = getSrchApiResultSet("MATCH_ALL", "test-openkor", "bbs", "tk_note", "","date_created",SortOrder.DESC, 100,0);
		return rsSetVo;
				
	}
		
	public Bbs getDocBbs(String argId) throws Exception {
		
		GetResponse resp = svcEsClient.prepareGet("test-openkor","bbs", argId).get();
		Map<String,Object>  mapSource = resp.getSource();
		
		Bbs bbs = new Bbs();
		bbs.setId(resp.getId());
		bbs.setSubject((String)mapSource.get("subject"));
		bbs.setNote((String)mapSource.get("note"));		
						
		return bbs;
	}
	
	public Map<String,Object> getDoc(String argIndex, String argType, String argId) throws Exception {
		
		GetResponse resp = svcEsClient.prepareGet(argIndex,argType, argId).get();
		Map<String,Object>  mapSource = resp.getSource();
		mapSource.put("_id", resp.getId());
		
		return mapSource;
	}
	
	
	public void insert(Bbs argBbs) throws Exception {
		
		String strId = argBbs.getId();
		if(strId==null || strId.equals("0") || strId.equals("")) {
			strId = null;
		}
		
		IndexResponse resp = svcEsClient.prepareIndex("test-openkor","bbs", strId)
								.setSource(
									jsonBuilder()
										.startObject()
											.field("subject",argBbs.getSubject())
											.field("note",argBbs.getNote())
											.field("tk_note",argBbs.getNote())
											.field("date_created", new Date())
										.endObject()	
								).get();
				
		logger.debug("# resp.getId(): " + resp.getId());
		svcEsClient.admin().indices().prepareRefresh("test-openkor").get();
	}

	
	public void save(String argIndex, String argType, Bbs argBbs) throws Exception {
		
		String strId = argBbs.getId();
		if(strId==null || strId.equals("0") || strId.equals("")) {
			strId = null;
		}
		
		IndexResponse resp = svcEsClient.prepareIndex(argIndex,argType, strId)
								.setSource(
									jsonBuilder()
										.startObject()
											.field("subject",argBbs.getSubject())
											.field("note",argBbs.getNote())
											.field("tk_note",argBbs.getNote())
											.field("date_created", new Date())
										.endObject()	
								).get();
				
		logger.debug("# resp.getId(): " + resp.getId());
		svcEsClient.admin().indices().prepareRefresh(argIndex).get();
	}	
	
	
	public void delete(String argId) throws Exception {		
		DeleteResponse resp = svcEsClient.prepareDelete("test-openkor","bbs", argId).get();				
		logger.debug("# resp.status().toString(): " + resp.status().toString());
		svcEsClient.admin().indices().prepareRefresh("test-openkor").get();
	}
	

	public void delete(String argIndex, String argType, String argId) throws Exception {
		
		DeleteResponse resp = svcEsClient.prepareDelete(argIndex, argType, argId).get();				
		
		logger.debug("# resp.status().toString(): " + resp.status().toString());
		svcEsClient.admin().indices().prepareRefresh("test-openkor").get();
	}
		
	
	public EsResSetVo getSrchApiResultSet (String argSrchCmd, String argSrchIndex, String argSrchType, String argSrchField, String argSrchKey, String argSortField, SortOrder argSortOrder, int argPageSize, int argPageNo) throws Exception {
		
		EsResSetVo  rsVo = new EsResSetVo();
		
		/* 1. set query builder type */
		
		QueryBuilder qb = null;
		
		if(argSrchCmd.toUpperCase().equals("MATCH_ALL")) {
			qb = matchAllQuery();
			
		} else if(argSrchCmd.toUpperCase().equals("MATCH")) {
			qb = matchQuery(argSrchField,argSrchKey);
			
		} else if(argSrchCmd.toUpperCase().equals("TERM")) {
			qb = termQuery(argSrchField,argSrchKey);
						
		} else if(argSrchCmd.toUpperCase().equals("WILDCARD")) {
			qb = wildcardQuery(argSrchField,argSrchKey);
			
		} else if(argSrchCmd.toUpperCase().equals("REGEXP")) {
			qb = regexpQuery(argSrchField,argSrchKey);
			
		} else {
			logger.debug("# CMD ERROR: matchQuery is running ");
			qb = matchQuery(argSrchField,argSrchKey);
		}		
		
		
		/* 3. Set HightLight */
		
		HighlightBuilder hb = new HighlightBuilder().preTags("<b style='color:red;'>").postTags("</b>").field(argSrchField);
		
		/* 4. Get Search Response */
		
		SearchResponse sr = svcEsClient.prepareSearch(argSrchIndex).setTypes(argSrchType)
								.setQuery(qb).highlighter(hb)
								.addSort(argSortField, argSortOrder)
								.setFrom(argPageNo).setSize(argPageSize).setExplain(true).get();
		
		/* 5. Set Total Hit Count */
		
		rsVo.setTotalHitCount(sr.getHits().getTotalHits());
		logger.debug("# sr.getHits().getTotalHits(): " + sr.getHits().getTotalHits() );
		
		/* 6. Set Highlight filed */
		
		List<Map<String,Object>> listMapSource = new ArrayList<Map<String,Object>>();
		
		for(SearchHit item : sr.getHits()) {
			Map<String,Object> mapSrc = item.getSource();
			mapSrc.put("HGLT_VALUE", item.getHighlightFields().get(argSrchField));
			mapSrc.put("_id",item.getId());
			listMapSource.add(mapSrc);
		}		
		
		rsVo.setListMapSrchResp(listMapSource);
		
		/* 6. Return VO */
		
		return rsVo;		
	}

	
	public EsResSetVo getSrchApiResultSetMatchTokenOption(String argSrchCmd, String argSrchIndex, String argSrchType, String argSrchField, String argSrchKey,String argAnalyzer, int argPageSize, int argPageNo) throws Exception {
		
		EsResSetVo  rsVo = new EsResSetVo();
		
		/* 1. set query builder type */
		
		QueryBuilder qb = null;
		
		if(argSrchCmd.toUpperCase().equals("MATCH_ALL")) {
			qb = matchAllQuery();
			
		} else if(argSrchCmd.toUpperCase().equals("MATCH")) {
			
			if(argAnalyzer==null || argAnalyzer.equals("")) {
				qb = matchQuery(argSrchField,argSrchKey);
			} else {
				qb = matchQuery(argSrchField,argSrchKey).analyzer(argAnalyzer);
			}
						
		} else if(argSrchCmd.toUpperCase().equals("TERM")) {
			qb = termQuery(argSrchField,argSrchKey);
						
		} else if(argSrchCmd.toUpperCase().equals("WILDCARD")) {
			qb = wildcardQuery(argSrchField,argSrchKey);
			
		} else if(argSrchCmd.toUpperCase().equals("REGEXP")) {
			qb = regexpQuery(argSrchField,argSrchKey);
			
		} else {
			logger.debug("# CMD ERROR: matchQuery is running ");
			qb = matchQuery(argSrchField,argSrchKey);
		}		
				
		/* 3. Set HightLight */
		
		HighlightBuilder hb = new HighlightBuilder().preTags("<b style='color:red;'>").postTags("</b>").field(argSrchField);
		
		/* 4. Get Search Response */
		
		SearchResponse sr = svcEsClient.prepareSearch(argSrchIndex).setTypes(argSrchType)
														.setQuery(qb).highlighter(hb)
														.setFrom(argPageNo).setSize(argPageSize).setExplain(true).get();
		
		/* 5. Set Total Hit Count */
		
		rsVo.setTotalHitCount(sr.getHits().getTotalHits());
		logger.debug("# sr.getHits().getTotalHits(): " + sr.getHits().getTotalHits() );
		
		/* 6. Set Highlight filed */
		
		List<Map<String,Object>> listMapSource = new ArrayList<Map<String,Object>>();
		
		for(SearchHit item : sr.getHits()) {
			Map<String,Object> mapSrc = item.getSource();
			mapSrc.put("HGLT_VALUE", item.getHighlightFields().get(argSrchField));
			mapSrc.put("_id",item.getId());
			listMapSource.add(mapSrc);
		}		
		
		rsVo.setListMapSrchResp(listMapSource);
		
		/* 6. Return VO */
		
		return rsVo;		
	}

	
}
